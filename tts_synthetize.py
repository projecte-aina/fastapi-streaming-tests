from urllib.parse import urlencode
from urllib.request import urlopen

def generate(text, speaker="f_sep_31", endpoint='http://0.0.0.0:8000'):
    q = {'speaker_id': speaker, 'text': text}
    audio_endpoint = f"{endpoint}/api/tts?{urlencode(q)}"
    audio = urlopen(audio_endpoint).read()
    return audio
