from fastapi import FastAPI
from starlette.responses import HTMLResponse
from starlette.websockets import WebSocket

from tts_synthetize import generate
import asyncio
import multiprocessing
import uvicorn
import queue

app = FastAPI()

html= """
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>WebSocket Audio</title>
</head>
<body>
    <h1>WebSocket Audio Streaming</h1>
    <button id="start">Iniciar</button>
    <button id="stop">Detener</button>
    <script>
        const startButton = document.getElementById('start');
        const stopButton = document.getElementById('stop');
        let audioContext;
        let source;
        let socket;
        let lastSource;
        let isPlaying = false;

        async function start() {
            audioContext = new (window.AudioContext || window.webkitAudioContext)();
            source = audioContext.createBufferSource();

            socket = new WebSocket('ws://' + location.host + '/audio-stream');
            socket.binaryType = 'arraybuffer';

            socket.onmessage = async (event) => {
                if (isPlaying) return;

                const audioData = new Uint8Array(event.data);
                const buffer = await audioContext.decodeAudioData(audioData.buffer);
                const newSource = audioContext.createBufferSource();
                newSource.buffer = buffer;
                newSource.connect(audioContext.destination);
                newSource.start();
                lastSource = newSource;
                isPlaying = true;

                newSource.onended = () => {
                    isPlaying = false;
                    sendFinishedPlaying(); // Send message indicating audio has finished playing
                };
            };

            socket.onopen = (event) => {
                console.log('WebSocket connected:', event);
            };

            socket.onclose = (event) => {
                console.log('WebSocket disconnected:', event);
                if (source) {
                    source.disconnect();
                }
            };

            socket.onerror = (event) => {
                console.error('WebSocket error:', event);
            };

            startButton.disabled = true;
            stopButton.disabled = false;
        }

        function stop() {
            if (socket) {
                socket.close();
            }

            if (lastSource) {
                lastSource.stop();
                lastSource.disconnect();
            }

            startButton.disabled = false;
            stopButton.disabled = true;
        }

        function sendFinishedPlaying() {
            socket.send('finished-playing');
        }

        startButton.addEventListener('click', start);
        stopButton.addEventListener('click', stop);
    </script>
</body>
</html>
"""





@app.get("/")
async def get():
    return HTMLResponse(html)


@app.get("/")
async def get():
    return HTMLResponse(html)

async def play_audio(queue: asyncio.Queue, websocket: WebSocket):
    while True:
        # get the next audio chunk from the queue
        audio_chunk = await queue.get()

        # check if this is the end of the stream
        if audio_chunk is None:
            break

        # send the audio chunk to the client
        await websocket.send_bytes(audio_chunk)
        # print a message for debugging
        print(f"Sent audio chunk of {len(audio_chunk)} bytes")
        # receive any data from the client (this will return None if the connection is closed)
        # TODO needs a timeout here in case the audio is not played (or finished?) within a given time
        data = await websocket.receive()
        # check if the connection is closed
        if data is None:
            break

@app.websocket_route("/audio-stream")
async def stream_audio(websocket: WebSocket):
    await websocket.accept()

    long_text = "Les institucions espanyoles han aprovat seixanta-dues noves normes legals que reforcen el castellà i discriminen directament o indirectament el català. Així ho assenyala la Plataforma per la Llengua que, com cada tres mesos, presenta un recull de totes les normes aprovades per l’executiu espanyol i les analitza. Els àmbits que van acumular més normes discriminatòries són l’etiquetatge i instruccions i els requisits en la petició d’ajuts i la candidatura a premis."


    try:
        # create the queue and the player task
        audio_queue = asyncio.Queue()
        player_task = asyncio.create_task(play_audio(audio_queue, websocket))

        sentences = long_text.split('.')
        # create a process pool
        pool = multiprocessing.Pool()
        # generate the audio for each sentence
        contents = pool.map(generate, [sentence.strip() + '.' for sentence in sentences if sentence])
        # add the audio to the queue in the same order as the sentences
        for content in contents:
            await audio_queue.put(content)

        # add a None item to the queue to signal the end of the stream
        await audio_queue.put(None)

        # wait for the player task to finish
        await player_task
    except Exception as e:
        print(f"Error: {e}")
    finally:
        await websocket.close()

def main():
    uvicorn.run(app, host="127.0.0.1", port=8008)


if __name__ == "__main__":
    main()
