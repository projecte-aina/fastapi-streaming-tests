import math

def generate_audio_signal():
    sample_rate = 44100  # 44.1 kHz
    duration = 1.0 / sample_rate
    frequency = 440.0  # A4
    amplitude = 0.5

    t = 0.0
    while True:
        # Calculate the next sample value
        sample = amplitude * math.sin(2.0 * math.pi * frequency * t)
        
        # Convert the sample to a 16-bit signed integer
        sample_int = int(sample * 32767.0)
        sample_bytes = sample_int.to_bytes(2, byteorder='little', signed=True)
        
        yield sample_bytes

        t += duration
