# FastAPI streaming tests

(After the installation) To launch the test and app:

```
uvicorn test:app --reload
```
For the echo websocket and

```
uvicorn app:app --reload
```
for the audio test websocket

Afterwards to connect to the test echo server one can do:

```
wscat -c ws://127.0.0.1:8000/ws
```
